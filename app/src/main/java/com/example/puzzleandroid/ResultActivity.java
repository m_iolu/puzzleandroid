package com.example.puzzleandroid;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.puzzleandroid.database.Result;
import com.example.puzzleandroid.database.ResultViewModel;

import java.util.List;

public class ResultActivity extends AppCompatActivity
{

    private ResultViewModel mResultViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final ResultListAdapter adapter = new ResultListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mResultViewModel = ViewModelProviders.of(this).get(ResultViewModel.class);

        mResultViewModel.getAllResults().observe(this, new Observer<List<Result>>()
        {
            @Override
            public void onChanged(@Nullable final List<Result> results)
            {
                // Update the cached copy of the results in the adapter.
                adapter.setResults(results);
            }
        });
    }
}
