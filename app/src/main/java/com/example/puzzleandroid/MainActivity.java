package com.example.puzzleandroid;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.puzzleandroid.database.Result;
import com.example.puzzleandroid.database.ResultViewModel;
import com.example.puzzleandroid.model.Game;

import java.util.Observable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, java.util.Observer
{
    private Game game;
    private Button[] buttons;
    private Button restartButton;
    private TextView textView;
    private final static int GAME_SIZE = 4;

    private ResultViewModel mResultViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mResultViewModel = ViewModelProviders.of(this).get(ResultViewModel.class);

        if(savedInstanceState == null)
            game = new Game(GAME_SIZE);
        else
        {
            String str = savedInstanceState.getString("gameState");
            game = new Game(str);
        }

        buttons = new Button[GAME_SIZE * GAME_SIZE];

        boolean gameOver = game.gameOver();

        for(int i=0;i<GAME_SIZE * GAME_SIZE;i++)
        {
            int id = getResources().getIdentifier("button"+i, "id", getPackageName());
            buttons[i] = findViewById(id);
            buttons[i].setTag(new Integer(i));
            buttons[i].setOnClickListener(this);
            if(gameOver)
                buttons[i].setEnabled(false);
        }

        textView = findViewById(R.id.textView);

        restartButton = findViewById(R.id.restartGameButton);
        restartButton.setOnClickListener(this);

        game.addObserver(this);

        //redraw everything on the window
        update(game, null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch(id)
        {
            case R.id.action_restart_game:
            {
                for(Button b : buttons)
                    b.setEnabled(true);
                game.restartGame();

                return true;
            }
            case R.id.action_settings:
            {
                Intent intent = new Intent(this, ResultActivity.class);
                startActivity(intent);

                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState)
    {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putString("gameState", game.toString());
    }

    @Override
    public void onClick(View v)
    {
        if(restartButton == v)
        {
            for(Button b : buttons)
                b.setEnabled(true);
            game.restartGame();
            return;
        }

        int x = (Integer)v.getTag();

        int lin = x / GAME_SIZE;
        int col = x % GAME_SIZE;

        if(!game.makeMove(lin, col))
            Toast.makeText(this, "Could not make move", Toast.LENGTH_SHORT).show();

        if(game.gameOver())
        {
            Toast.makeText(this, "You won !!!", Toast.LENGTH_LONG).show();

            for(Button b : buttons)
                b.setEnabled(false);

            Result result = new Result("Andrei", "2019/05/10", 100);
            mResultViewModel.insert(result);
        }
    }

    @Override
    public void update(Observable o, Object arg)
    {
        int[][] mat = game.getGameMatrix();

        for(int i=0;i<buttons.length;i++)
        {
            int lin = i / GAME_SIZE;
            int col = i % GAME_SIZE;

            if(mat[lin][col] == 0)
                buttons[i].setText("");
            else
                buttons[i].setText(""+mat[lin][col]);
        }

        textView.setText("Number of moves until now: " + game.getNumberOfMoves());
    }
}
