package com.example.puzzleandroid.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

public class Game extends java.util.Observable
{
	private final static boolean GENERATE_RANDOM = false;
	
	private int[][] a;
	private int size;
	private int numberOfMoves;
	
	public Game(int size)
	{
		this.size = size;
		a = new int[size][size];

		init();
	}

	public Game(String str)
	{
		StringTokenizer st = new StringTokenizer(str, " ");

		size = Integer.parseInt(st.nextToken());

		a = new int[size][size];

		for(int i=0;i<size;i++)
			for(int j=0;j<size;j++)
				a[i][j] = Integer.parseInt(st.nextToken());

		numberOfMoves = Integer.parseInt(st.nextToken());
	}

	public void restartGame()
	{
		init();
		setChanged();
		notifyObservers();
	}

	private void init()
	{
		numberOfMoves = 0;

		List<Integer> puzzle = new ArrayList<>();

		for(int i=1;i<size*size;i++)
			puzzle.add(i);

		puzzle.add(size * size - 2, 0);

		if(GENERATE_RANDOM)
		{
			do
			{
				Collections.shuffle(puzzle);
			}
			while(!PuzzleUtils.isSolvable(puzzle));
		}

		for (int i = 0; i < puzzle.size(); ++i)
			a[i / size][i % size] = puzzle.get(i);
	}
	
	public boolean makeMove(int row, int col)
	{
		int px0 = 0, py0 = 0;
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				if (a[i][j] == 0)
				{
					px0 = i;
					py0 = j;
				}

		if (row != px0 && col != py0)
			return false;

		if (row == px0 && col == py0)
			return false;

		if (row == px0)
		{
			if (col < py0)
			{
				for (int j = py0 - 1; j >= col; j--)
					a[row][j + 1] = a[row][j];
				a[row][col] = 0;
			} 
			else
			{
				for (int j = py0 + 1; j <= col; j++)
					a[row][j - 1] = a[row][j];
				a[row][col] = 0;
			}
		}

		if (col == py0)
		{
			if (row < px0)
			{
				for (int i = px0 - 1; i >= row; i--)
					a[i + 1][col] = a[i][col];
				a[row][col] = 0;
			} 
			else
			{
				for (int i = px0 + 1; i <= row; i++)
					a[i - 1][col] = a[i][col];
				a[row][col] = 0;
			}
		}
		
		numberOfMoves++;
		
		setChanged();
		notifyObservers();

		return true;
	}

	public boolean gameOver()
	{
		for (int i = 0; i < size; i++)
			for (int j = 0; j < size; j++)
				if (a[i][j] != i * size + j + 1 && (i != size - 1 || j != size - 1))
					return false;
		return true;
	}
	
	public int getGameSize()
	{
		return size;
	}
	
	public int getNumberOfMoves()
	{
		return numberOfMoves;
	}

	public int[][] getGameMatrix()
	{
		return a;
	}

	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(size);

		for(int i=0;i<size;i++)
			for(int j=0;j<size;j++)
				sb.append(" ").append(a[i][j]);

		sb.append(" ").append(numberOfMoves);

		return sb.toString();
	}
}
